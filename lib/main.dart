import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Application name
      title: 'Flutter Hello World',
      // Application theme data, you can set the colors for the application as
      // you want
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      // A widget which will be started on application startup
      home: MyHomePage(title: 'Pratikum III Muhammad Ridha Asyqari'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  const MyHomePage({@required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // The title text which will be shown on the action bar
          leading: Icon(Icons.home),
        ),
        body: Container(
            decoration: BoxDecoration(
              color: const Color(0xff7c94b6),
              image: const DecorationImage(
                image: NetworkImage('https://1.bp.blogspot.com/-7UHBAW2ABtE/XxPjlQui6VI/AAAAAAAACAQ/B4P3GBf3DB4RjtmpO-4z4cL3WTFdmZr2wCLcBGAsYHQ/s1600/lukisan%2Babstrak%2Bbackground%2Btegaraya.com%2B2.jpg'),
                fit: BoxFit.cover,
              ),
              border: Border.all(
                color: Colors.black,
                width: 8,
              ),
              borderRadius: BorderRadius.circular(12),
            ),
            height: 280,
            width: 200,
            margin: EdgeInsets.all(20)));
  }
}
